﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class Item : ScriptableObject
{
    new public string name;
    public string category;
    public int amount;
    public int valuePerOne;
    public string description;
    public Sprite icon;
    public GameObject gameObject;
}
