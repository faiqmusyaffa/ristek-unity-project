﻿using UnityEngine.EventSystems;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    Vector3 movement;
    [SerializeField] private float playerSpeed = 30F;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        // Pause the game if inventory is opened (enabled)
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        // Player movement
        float xDirection = Input.GetAxis("Horizontal");
        float zDirection = Input.GetAxis("Vertical");
        Vector3 moveDirection = new Vector3(xDirection, 0F, zDirection).normalized;
        transform.position += moveDirection * playerSpeed * Time.deltaTime;
    
    }
}
