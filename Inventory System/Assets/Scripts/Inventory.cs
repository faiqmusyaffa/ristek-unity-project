﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one instance of Inventory found");
            return;
        }
        instance = this;
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int inventorySpace = 20;
    public List<Item> items = new List<Item>();

    public bool AddItem(Item item)
    {

        // Check inventory space
        if (items.Count >= inventorySpace)
        {
            Debug.Log("NOT ENOUGH SPACE");
            return false;
        }

        items.Add(item); // Add item

        // Invoke this method to indicate that an item was added
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }

        return true;
    }

    public void RemoveItem(Item item)
    {
        items.Remove(item); // Remove item

        // Invoke this method to indicate that an item was removed
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }
}
